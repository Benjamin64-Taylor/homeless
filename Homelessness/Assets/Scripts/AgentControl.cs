﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentControl : MonoBehaviour
{
	public float baseSpeed = 10.0f;
	public float speedVariance = 1.3f;
	public Transform[] home;
	public Transform[] target;
	private NavMeshAgent agent;
	public bool reverse = false;

    // Start is called before the first frame update
    void Start()
    {
		
		agent = this.GetComponent<NavMeshAgent>();
		if (!reverse){
			agent.SetDestination(target[(Random.Range(0, target.Length))].position);
		} else {
			agent.SetDestination(home[(Random.Range(0, home.Length))].position);

		}

		if(Random.Range(0,2) == 0){
			agent.speed = baseSpeed - Random.Range(0.0f, speedVariance);
		} else {
			agent.speed = baseSpeed + Random.Range(0.0f, speedVariance);
		}
    }

	void Update(){
		if(!agent.pathPending) {
			if(agent.remainingDistance <= agent.stoppingDistance) {
				//if(!agent.hasPath || agent.velocity.sqrMagnitude == 0f) {
				if(!reverse){
					Transform newHome = home[Random.Range(0, home.Length)];
					gameObject.transform.position = new Vector3(newHome.position.x, gameObject.transform.position.y, newHome.position.z); 
					agent.SetDestination(target[(Random.Range(0, target.Length))].position);

				} else {
					Transform newHome = target[Random.Range(0, target.Length)];
					gameObject.transform.position = new Vector3(newHome.position.x, gameObject.transform.position.y, newHome.position.z); 
					agent.SetDestination(target[(Random.Range(0, home.Length))].position);
				}

			}
		}
					
	}
}
