﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutScene : MonoBehaviour
{

	GameObject PlayerObj;
	PlayerMovement playerMovement;
	PlayerHealth playerHealth;
	public float cutSceneTimer = 5.0f;
	public bool cutSceneHit = false;
	public GameObject CutsceneNPC;
	PurseNPC CutSceneNPCScript;
    // Start is called before the first frame update
    void Start()
    {
		PlayerObj = GameObject.FindGameObjectWithTag("Player");
		playerMovement = PlayerObj.GetComponent<PlayerMovement>();
		playerHealth = PlayerObj.GetComponent<PlayerHealth>();
		CutSceneNPCScript = CutsceneNPC.GetComponent<PurseNPC>();
    }

    // Update is called once per frame
    void Update()
    {
		if(cutSceneHit) {
			cutSceneTimer = cutSceneTimer - Time.deltaTime;
		}
		if(cutSceneTimer < 0.0f){
			cutSceneHit = false;
			playerMovement.enabled = true;
			playerHealth.inCutscene = false;
			Destroy(gameObject);
		}
    }

	void OnTriggerEnter (Collider other) {
		if (other.gameObject.tag == "Player") {
			playerMovement.rb.velocity = Vector3.zero;
			playerMovement.enabled = false;
			cutSceneHit = true;
			playerHealth.inCutscene = true;
			CutSceneNPCScript.hitCutScene = true;
		}
	}
}
