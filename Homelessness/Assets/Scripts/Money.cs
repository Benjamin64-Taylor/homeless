﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Money : MonoBehaviour
{
	public float moneyAmount;
	GameObject PlayerObj;
	UI ui;
	public GameObject bedHitbox;
    // Start is called before the first frame update
    void Start()
    {
		PlayerObj = GameObject.FindGameObjectWithTag("Player");
		ui = PlayerObj.GetComponent<UI>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	void OnTriggerEnter (Collider other) {
		if (other.gameObject.tag == "Player") {
			ui.money = ui.money + moneyAmount;
			if(moneyAmount >= 100){
				bedHitbox.SetActive(true);
			}
			Destroy(gameObject);
		}
	}
}
