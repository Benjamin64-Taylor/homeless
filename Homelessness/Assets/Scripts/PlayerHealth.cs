﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
	public bool isDead;
	public float playerHealth;
	public float playerMaxHealth = 100.0f;
	public Slider healthSlider;
	public Transform lastSquareEntrance;
	public float healthWhenEntered;

	bool slowDown = false;
	public bool inCutscene = false;
	public bool safeRoom = false;
	PlayerMovement playerMovement;
	UI ui;


    // Start is called before the first frame update
    void Start()
    {
		isDead = false;
		playerHealth = playerMaxHealth;
		healthSlider.value = playerHealth;

		playerMovement = GetComponent<PlayerMovement>();
		ui = GetComponent<UI>();
		lastSquareEntrance = this.transform;
		healthWhenEntered = playerMaxHealth;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
		if (!inCutscene) {
			if(!safeRoom) {
				if(Time.timeScale != 0.0f){
					playerHealth = playerHealth - 0.06f;
					if(Input.GetKey(KeyCode.LeftShift)) {
						playerHealth = playerHealth - 0.08f;
					}
				}
			}
		}
		healthSlider.value = playerHealth;

		if(playerHealth <= 0 && !isDead) {
			Death();
		}
		if (slowDown) {
			if(Time.timeScale != 0){
				Time.timeScale = Time.timeScale - 0.02f;
			}
			if (Time.timeScale < 0.1f) {
				Time.timeScale = 0.0f;
			}
		} 
		//if (!slowDown) {
		//	Time.timeScale = 1.0f;
		//}
    }

	void Death() {
		playerMovement.enabled = false;
		ui.Death();
		slowDown = true;
		isDead = true;
	}

	public void Respawn() {
		playerHealth = Mathf.Max(healthWhenEntered, 20.0f);
		this.transform.position = lastSquareEntrance.position;
		playerMovement.enabled = true;
		slowDown = false;
		isDead = false;
		Time.timeScale = 1.0f;
	}

	public void AddHealth(int amount){
		playerHealth = Mathf.Min(playerHealth + amount, playerMaxHealth);
	}
}