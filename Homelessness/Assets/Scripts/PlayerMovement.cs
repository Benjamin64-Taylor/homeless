﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
	public float speed = 10.0f;
	public float runSpeed = 15.0f;
	public Rigidbody rb;
	NavMeshObstacle navMeshObstacle;
	public Transform cameraTransform;
	private Vector3 moveDirection = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
		rb = GetComponent<Rigidbody>();   
		navMeshObstacle = GetComponent<NavMeshObstacle>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
		moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		moveDirection = cameraTransform.TransformDirection(moveDirection);

		if(Input.GetKey(KeyCode.LeftShift)) {
			moveDirection *= runSpeed;
		}else{
			moveDirection *= speed;
		}
		rb.velocity = moveDirection;
	}
}
