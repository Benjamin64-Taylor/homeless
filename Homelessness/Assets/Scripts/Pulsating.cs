﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulsating : MonoBehaviour
{
	public float approachSpeed = 0.02f;
	public float growthBound = 1.5f;
	public float shrinkBound = 0.7f;
	private float currentRatio = 0.5f;

	private Coroutine routine;
	private bool keepGoing = true;
	private bool closeEnough = false;

	void Awake() {
		this.routine = StartCoroutine(this.Pulse());
	}

	IEnumerator Pulse(){
		while(keepGoing){
			while(this.currentRatio != this.growthBound){
				yield return new WaitForSeconds(0.005f);
				currentRatio = Mathf.MoveTowards( currentRatio, growthBound, approachSpeed);
				this.transform.localScale = Vector3.one * currentRatio;
				yield return new WaitForEndOfFrame();
			}

			// Shrink for a few seconds
			while (this.currentRatio != this.shrinkBound){
				yield return new WaitForSeconds(0.005f);

				// Determine the new ratio to use
				currentRatio = Mathf.MoveTowards( currentRatio, shrinkBound, approachSpeed);
				this.transform.localScale = Vector3.one * currentRatio;
				// Update our text element

				yield return new WaitForEndOfFrame();
			}

		}
	}
				

}
