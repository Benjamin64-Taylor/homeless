﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PurseNPC : MonoBehaviour
{

	public float baseSpeed = 10.0f;
	public Transform target;
	private NavMeshAgent agent;
	public bool hitCutScene = false;

    // Start is called before the first frame update
    void Start()
    {
		agent = this.GetComponent<NavMeshAgent>();

    }

    // Update is called once per frame
    void Update()
    {
		if(hitCutScene){
			agent.SetDestination(target.position);
			if(!agent.pathPending) {
				if(agent.remainingDistance <= agent.stoppingDistance) {
					Destroy(gameObject);
				}
			}
		}
    }
}
