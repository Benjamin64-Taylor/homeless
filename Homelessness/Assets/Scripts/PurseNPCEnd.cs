﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurseNPCEnd : MonoBehaviour
{

	UI ui;
	LogFile log;
    // Start is called before the first frame update
    void Start()
    {
		ui = GameObject.FindGameObjectWithTag("Player").GetComponent<UI>();
		log = GameObject.FindGameObjectWithTag("Player").GetComponent<LogFile>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	void OnTriggerEnter (Collider other) {
		Debug.Log("Something entered with tag " + other.gameObject.tag);
		if (other.gameObject.tag == "Player") {
			ui.win();
			log.WriteLine(transform.parent.parent.name, transform.parent.parent.name, ui.money, ui.respawnCount, Time.time, "Returned");
		}
	}
}
