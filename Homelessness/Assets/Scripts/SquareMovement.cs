﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareMovement : MonoBehaviour
{
	private Transform Camera;
	private Transform Player;
	public Transform sendPos;
	public Transform sendCamPos;
	GameObject PlayerObj;
	PlayerHealth playerHealth;
	UI ui;

	private bool hasNotProgressed = true;

	public LogFile log;
    void Start()
    {
		Camera = GameObject.FindGameObjectWithTag("MainCamera").transform;
		Player = GameObject.FindGameObjectWithTag("Player").transform;
		PlayerObj = GameObject.FindGameObjectWithTag("Player");
		playerHealth = PlayerObj.GetComponent<PlayerHealth>();
		ui = PlayerObj.GetComponent<UI>();
		log = PlayerObj.GetComponent<LogFile>();
	}


    // Update is called once per frame
    void Update()
    {
        
    }

	void OnTriggerEnter (Collider other) {
		Debug.Log("Something entered with tag " + other.gameObject.tag);
		if (other.gameObject.tag == "Player") {
			Player.position = sendPos.position;
			Camera.position = sendCamPos.position;
			//playerHealth.lastSquareEntrance = sendPos;
			//playerHealth.healthWhenEntered = playerHealth.playerHealth;
			playerHealth.playerHealth = playerHealth.playerMaxHealth;
			//if(hasNotProgressed) {
				if (log != null){
					string logString = Time.time + transform.parent.parent.name + sendPos.parent.parent.name;
					Debug.Log(logString);
					log.WriteLine(transform.parent.parent.name, sendPos.parent.parent.name, ui.money, ui.respawnCount, Time.time, "Playing");
					Debug.Log("Written to logfile");
				}
				hasNotProgressed = false;
			//}
			ui.respawnCount = 0;
		}
	}
}
