﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThinkingTextUpdate : MonoBehaviour
{
	UI ui;
	public string updateText;
	public bool notSaidYet = true;
    // Start is called before the first frame update
    void Start()
    {
		ui = GameObject.FindGameObjectWithTag("Player").GetComponent<UI>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

	void OnTriggerEnter (Collider other) {
		Debug.Log("Something entered with tag " + other.gameObject.tag);
		if (other.gameObject.tag == "Player") {
			if(notSaidYet) {
				ui.thinkingText.text = updateText;
			}
			notSaidYet = false;
		}
	}
}
