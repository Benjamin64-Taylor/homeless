﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TrafficLights : MonoBehaviour
{
	//public NavMeshSurface surface;
	public float crossingTime = 5.0f;
	public float stallTime = 4.0f;
	float currentTime; 
	bool onStallTime = true;
	public GameObject[] barriers;
    // Start is called before the first frame update
    void Start()
    {
		currentTime = stallTime;
    }

    // Update is called once per frame
    void Update()
    {
		currentTime = currentTime - Time.deltaTime;
		if(currentTime < 0.0f) {
			onStallTime = Flip(onStallTime);

		}
    }

	bool Flip(bool stall){
		if (stall) {
			currentTime = crossingTime;
			for (int i = 0; i < barriers.Length; i++){
				barriers[i].transform.position = barriers[i].transform.position + (Vector3.down*3);
			}
		} else {
			currentTime = stallTime;
			for (int i = 0; i < barriers.Length; i++){
				barriers[i].transform.position = barriers[i].transform.position + (Vector3.up*3);
			}
		}
		//surface.BuildNavMesh();
		return (!stall);
	}
}
