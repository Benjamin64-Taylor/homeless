﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{
	public Text thinkingText;
	public Button gameOverButton;
	public Button restartButton;
	public Text gameOverText;
	PlayerHealth playerHealth;
	GameObject PlayerObj;
	public float money = 5.0f ;
	public Text moneyText;

	public Button ContinueButton;
	public Text PauseText;
	public bool isPaused;

	public Button CheapFood;
	public Button MediumFood;
	public Button ExpensiveFood;
	public Button Leave;
	public Text RestuarantText;
	PlayerMovement playerMovement;

	public int respawnCount = 0;

	public Text winText;
	public Text bedText;
	public Button replayButton;

    // Start is called before the first frame update
    void Start()
    {
		ContinueButton.gameObject.SetActive(true);
		PauseText.gameObject.SetActive(true);
		isPaused = true;
		Time.timeScale = 0.0f;

		gameOverButton.gameObject.SetActive(false);
		restartButton.gameObject.SetActive(false);
		gameOverText.gameObject.SetActive(false);
		PlayerObj = GameObject.FindGameObjectWithTag("Player");
		playerHealth = PlayerObj.GetComponent<PlayerHealth>();
		playerMovement = GetComponent<PlayerMovement>();


		CheapFood.gameObject.SetActive(false);
		MediumFood.gameObject.SetActive(false);
		ExpensiveFood.gameObject.SetActive(false);
		Leave.gameObject.SetActive(false);
		RestuarantText.gameObject.SetActive(false);

		winText.gameObject.SetActive(false);
		replayButton.gameObject.SetActive(false);
		bedText.gameObject.SetActive(false);

		thinkingText.text = ("Another long night on the streets, better get some food before I get too hungry.");

    }

    // Update is called once per frame
    void Update()
    {
		moneyText.text = "$" + money.ToString();
		if(Input.GetKeyDown(KeyCode.Escape)) {
			if(!playerHealth.isDead){
			pause();
			}
		}
    }

	public void pause(){
		if(isPaused) { //Already paused, unpause
			ContinueButton.gameObject.SetActive(false);
			PauseText.gameObject.SetActive(false);
			isPaused = !isPaused;
			Time.timeScale = 1.0f;

		} else { //Not yet paused, pause
			ContinueButton.gameObject.SetActive(true);
			PauseText.gameObject.SetActive(true);
			isPaused = !isPaused;
			Time.timeScale = 0.0f;
		}
	}

	public void OnContinueClick(){
		ContinueButton.gameObject.SetActive(false);
		PauseText.gameObject.SetActive(false);
		pause();
	}

	public void OnRespawnClick(){
		gameOverButton.gameObject.SetActive(false);
		gameOverText.gameObject.SetActive(false);
		restartButton.gameObject.SetActive(false);
		respawnCount = respawnCount + 1;
		playerHealth.Respawn();
	}

	public void OnRetryClick(){
		Scene scene = SceneManager.GetActiveScene();
		SceneManager.LoadScene(scene.name);
	}

	public void Death() {
		gameOverButton.gameObject.SetActive(true);
		gameOverText.gameObject.SetActive(true);
		restartButton.gameObject.SetActive(true);

	}

	public void Restuarant() {
		CheapFood.gameObject.SetActive(true);
		MediumFood.gameObject.SetActive(true);
		ExpensiveFood.gameObject.SetActive(true);
		Leave.gameObject.SetActive(true);
		RestuarantText.gameObject.SetActive(true);
		playerHealth.safeRoom = true;
		playerMovement.enabled = false;
	}

	public void OnCheapClick() {
		if(money >= 1) {
			money = money -1;
			playerMovement.speed = playerMovement.speed * 1.01f;
			playerMovement.runSpeed = playerMovement.runSpeed * 1.01f;

		}
	}

	public void OnMediumClick() {
		if (money >= 3){
			money = money -3;
			playerMovement.speed = playerMovement.speed * 1.05f;
			playerMovement.runSpeed = playerMovement.runSpeed * 1.05f;
		}
	}

	public void OnExpensiveClick() {
		if(money >= 5){
			money = money -5;
			playerMovement.speed = playerMovement.speed * 1.08f;
			playerMovement.runSpeed = playerMovement.runSpeed * 1.08f;
		}
	}

	public void OnLeaveClick() {
		CheapFood.gameObject.SetActive(false);
		MediumFood.gameObject.SetActive(false);
		ExpensiveFood.gameObject.SetActive(false);
		Leave.gameObject.SetActive(false);
		RestuarantText.gameObject.SetActive(false);
		playerHealth.safeRoom = false;
		playerMovement.enabled = true;
	}

	public void win() {
		winText.gameObject.SetActive(true);
		replayButton.gameObject.SetActive(true);
	}

	public void bed() {
		bedText.gameObject.SetActive(true);
		replayButton.gameObject.SetActive(true);
	}
}
